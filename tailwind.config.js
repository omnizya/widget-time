/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    borderRadius: {
      none: '0',
      sm: '.125rem',
      default: '.25rem',
      lg: '.5rem',
      full: '9999px',
      xl: '12px'
    },
    fontFamily: {
      sans: ['"Product Sans"', 'sans-serif'],
      display: ['"Product Sans"', 'sans-serif'],
      body: ['"Product Sans"', 'sans-serif']
    }
  },
  variants: {},
  plugins: []
}
